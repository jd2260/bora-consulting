# Bora Consulting Website

## Tools

You will require 3 things to make changes to the website:

1. VSCode
2. SourceTree
3. A GitLab account

VSCode will be used to make changes, while SourceTree will track the changes you make and allow you to push them to production. 

## Hugo

Hugo is a static site generator - this means it loads content into templates. I've built the templates (found in layouts folder) for your websites needs.

Intructions for getting started with Hugo can be found [here](https://gohugo.io/getting-started/quick-start/)

All blogs & pages are managed via the content folder. To make a new blog make a new file in blogs, paste the following section into the file:

```
+++
title = "Insert Title Here"
date = "2021-01-23T13:50:46+02:00"
tags = [""]
categories = ["Articles"]
description = "Example description"
banner = "img/blog/picture.png"
+++
```

Place all blog pictures in the folder `static/img/blog/`.

## Theme

A good resources for how [config.toml](config.toml) works can be found [here](https://github.com/devcows/hugo-universal-theme)

All items on the front page, menu, and footer are manageable through `config.toml.` 

The carousel, certifications, clients and testimonials are managed in the data folder. Each have a `.yml` file. 