+++
title = "About"
id = "about"
+++

#### How do we achieve profitable and sustainable growth?

We created an agile, tech-savvy and boots-on-the-ground firm with this objective in mind. 

As a *boutique* consulting firm, **Bora Consulting** helps mining companies to achieve operational excellence. We bring our expertise in maintenance and reliability to improve equipment availability and increase production throughput. 

Our consulting framework is founded on 4 fundamentals of your business: People, Processes, Assets and Cash.

{{<rawhtml>}}
    <section>
        <img class="center" src="/img/fundamentals.png">
    </section>
{{</rawhtml>}}

Prior to founding **Bora Consulting**, Alp spent 10 years as operations and maintenance professional across Canada with mining companies, such as Rio Tinto, Vale, Tata Steel and Suncor. He is also a founding partner at [Greenlight Partners](https://greenlightpartners.nyc), a venture studio helping clean-tech startups through business development, capital and fundraising advisory.

Based on our proven approach in maintenance & reliability, we enabled mining and processing operations to achieve significant improvements:

🛠️ Reduce maintenance backlog by 25%  
📈 Improve OEE by up to 20%  
🎯 Identify the right spare parts at right cost  
🔧 20% Defect elimination over 12 months   
💰 Cost control and optimization  
🚀 Maintenance leadership coaching  
💎 Step change in safety performance  

**With a solid network of experienced professionals and partners, we guarantee a hands-on team with an entrepreneurial attitude.**
