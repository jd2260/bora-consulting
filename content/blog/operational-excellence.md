+++
title = "Operational Excellence & Maintenance"
date = "2021-04-05T13:50:46+02:00"
tags = ["process"]
categories = ["Articles"]
description = "Asset management and operational excellence are interlocking and identical programs"
banner = "img/blog/operational_excellence.png"
+++

Many organizations are considering or initiated an Operational Excellence program without any details to define the monster beyond the compelling term. 

The primary objective of operating companies is to **maximize production throughput safely while minimizing energy and material consumption and human costs**. In many cases, it is a competitive advantage. In the mining industry, it’s a matter of life and death.

My vision of Operational Excellence is built on 4 pillars: People, Processes, Assets and Cash. Every other practice and tools toward Operational Excellence falls in one of these pillars. 

{{<rawhtml>}}
    <br>
    <section>
        <img class="center" src="/img/blog/operational_excellence.png">
    </section>
{{</rawhtml>}}


One of the first questions to be asked for Operational Excellence roadmap is what is important to the business/mission: **Where we are and where want to be?** 

This should be spelled out in the business/mission strategy and objectives.

The value proposition of Operational Excellence is to **increase business value safely and sustainably.** It includes improving reliability, reducing risks and variation.  

In the mining and processing plants, **the asset management and operational excellence are interlocking and identical programs.** 

Once the roadmap toward Operational Excellence, to ensure a smooth implementation of the programs across the organization, the change management needs to be an integral part of framework. To do so, it is recommended to frequently administer surveys with people on the front lines to measure their willingness and understanding of the changes. 

If you are planning to implement operational excellence program or simply want to stabilize & increase your production throughput, **the main bottleneck is the execution**. Based on our multiple engagements in operating firms, getting things done in a sustainable way remains the challenge. 

Couple lessons learned based on previous continuous improvement projects toward Operational Excellence
- Ensure that the leadership is committed and aligned
- Gain acceptance across the entire organization (front-line supervisors are key to success!)
- Assign the best people to the improvement program
- Formulate and implement a clear & solid process for improvement
- Maintain focus on value to business /mission objectives ... identify & eliminate wastes
- Communicate frequently at all levels
- Train, re-train, reinforce and repeat.

#### *Learn more by [contacting us](/contact)*