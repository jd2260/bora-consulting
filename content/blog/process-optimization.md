+++
title = "Process Optimization"
date = "2021-03-13T13:50:46+02:00"
tags = ["process"]
categories = ["Articles"]
description = "A DMAIC Approach to Process Optimization"
banner = "img/blog/process_optimization.png"
+++

Process Optimization has been thrown around as a buzzword the last few years in heavy industry. From a process engineering standpoint, the first response you get when you say you are going to “optimize the process” is “exactly what are you optimizing?”

<!--more-->

{{<rawhtml>}}
    <section>
        <img class="center" src="/img/blog/process_optimization.png">
    </section>
{{</rawhtml>}}

A process has many key attributes to it including HSE, product quality, raw material costs and of course, profits. The golden rule of manufacturing is that we are in the business of making money so wouldn’t it be obvious to optimize the process for profits? Not necessarily. 

### Context

In one case of process optimization, a large pile of contaminated raw material needed to be recycled back into the system. The financial impact to the business was to avoid large hauling & disposal fees. There was also the opportunity to recover material costs previously spent on that material that would have otherwise be wasted. 

The potential cost savings of this project were in the millions – but recycling the material also had potential risks that needed to be mitigated.

### Approach

A cornerstone of Operational Excellence framework is Lean-Six-Sigma. In this context, we decided to leverage DMAIC (Define, Measure, Analyze, Improve, Control) approach to resolve this challenge, but in a permanent and sustainable matter. Then, we began to go through the steps. 

{{<rawhtml>}}
    <section>
        <img class="center" src="/img/blog/dmiac.png">
    </section>
{{</rawhtml>}}

#### Define 

The most crucial step! What exactly are we looking at and what is outside the scope of the process? The process was defined as introducing the contaminated raw material back in the production line. The scope involved the entire plant which required a comprehensive stakeholder management. One unmissable requirement was the quality of the final product must remain high… there was no room for error on that.

#### Measure

Start introducing quantifiable metrics in order make informed decisions on the process. Example metrics in this case included:

{{<rawhtml>}}
    <section>
        <img class="center" src="/img/blog/metrics.png">
    </section>
{{</rawhtml>}}

#### Analyze

Carefully explore feasibility of all your options pertaining to the process – nothing is out of the question. This is where 70 – 80% of the optimization takes place. Various tools are available to help with this step (plant models, machine learning, etc). In this case, a plant simulator was developed to analyze increased throughput, and additional wear and tear on processing infrastructure, which was identified to be a primary concern. 

#### Improve

Implement the proposed changes and continue to make minor improvements to the new process using what you learn during implementation. You might need to continually iterate between analyze, improve, and control a few times to achieve true optimization. Achieving the last 20-30% of optimization is difficult, but worth the effort in the long run. This meant trying out multiple points of entry for the contaminated material and proceeding with the best available option.

#### Control

The final, but commonly forgotten step. Too often a new process is introduced, and the engineer or the management team moves on to their next big project. Ensuring that the project is continually monitored until it becomes second nature is the most important step of process improvement and optimization. 

{{<rawhtml>}}
    <section>
        <img class="center" src="/img/blog/control.png">
    </section>
{{</rawhtml>}}

### Results

Using a DMAIC approach, we were able to identify multiple areas for re-introduction and chose the one that mitigated risks with a projected cost savings of $4.5 million dollars over 4 years. This required no large capital costs and was introduced at a point where many contaminants could be separated. This allowed cost savings to occur with minimal impact on existing processes.

When all is said and done, we optimize the process for reliable and steady-state operation of the plant. This is why controlling the process is such a critical step. As by-product, we set the conditions to generate profits, improve product quality and increase equipment uptime.

#### *Learn more by [contacting us](/contact)*