+++
title = "Tool Time Analysis"
date = "2021-03-14T13:50:46+02:00"
tags = ["people"]
categories = ["Articles"]
description = "From Assessment to Improvement with Wrench Time Analysis"
banner = "img/blog/contractor.jpg"
+++

On paper, the plant had a strong asset management infrastructure in place. The SAP Plant Maintenance module was fully integrated, strong maintenance plans were in place, and there was an appropriate maintainer to planner to supervisor ratio and strong engineering support.

<!--more-->

{{<rawhtml>}}
    <section>
        <img class="center" src="/img/blog/contractor.jpg">
    </section>
{{</rawhtml>}}

However, the plant was suffering high breakdown rates, poor schedule compliance, very low engagement, and the team felt like they were in a vicious cycle of firefighting. 

As a result, a thorough assessment of the maintenance team and maintenance management program was requested. 

#### Approach

After dozens of interviews with trades, maintenance managers, operators, and supervisors, we started to develop an understanding of the situation. 

Next, a thorough data analysis helped us to pinpoint several recurring issues. An analysis was performed on work order history, PM percentage vs. total available work hours, schedule compliance, and production KPIs. 

The analysis was further enhanced by on-site shadowing of supervisors and trades. We selected key dates for the shadowing (i.e., shutdown days) to assess the quality of shutdown management and observe the effectiveness of execution.


#### Actions in partnership

Tool time was a powerful metric because it helped to expose many of the teams’ weaknesses: engagement, respect of processes, communication between departments, and productivity of the maintenance members.

After the assessment and analysis, we recommended the following:

{{<rawhtml>}}
    <section>
        <img class="center" src="/img/blog/maitenance-plan.png">
    </section>
{{</rawhtml>}}

#### Results

With a SWAT team approach, we reduced the maintenance backlog by 20%, which allowed us to focus on high criticality assets. In less than eight weeks, we started to see a reduction in major breakdowns of equipment. We started performing root cause analysis on each breakdown and defect elimination workshops to achieve better reliability. 

By reducing waste related to maintenance activities in planning and execution, the team was able to devote more work hours to preventive tasks. These actions led to improved plant availability and higher engagement from the maintenance team. 

#### *Learn more by [contacting us](/contact)*