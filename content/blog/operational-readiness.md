+++
title = "Operational Readiness"
date = "2021-04-08T13:50:46+02:00"
tags = ["process"]
categories = ["Articles"]
description = "Turnkey Maintenance Program within 4 Months"
banner = "img/blog/truck.png"
+++

For the commissioning of an iron concentrator in northern Quebec, our client only had the engineering documents and equipment manuals. The entire asset management component had to be in place prior to commissioning. We only had four months to go! 

<!--more-->

Given the short preparation time, in partnership with our client, we developed a roadmap to build the maintenance component and we quickly started the work. 

{{<rawhtml>}}
    <br>
    <section>
        <img class="center" src="/img/blog/truck.png">
    </section>
{{</rawhtml>}}

We conducted workshops with the maintenance team to develop the most critical aspects, in order to support production and improve the operational performance of our client.

#### Workshops:
In order to create more value in the limited time available to us, we opted for three workshops:

1.	**Criticality analysis**: Perform structured criticality reviews of processes and assets

Criticality analysis is the starting point for developing a maintenance management program. We need to first slice and dice the work based on criticality. 

Our easy-to-use criticality matrix quickly identifies the assets based on the HSE and production risk to the business. One common mistake that we see in the industry is that the maintenance team often gets lost in the details. We have witnessed this analysis turning into a lengthy philosophical discussion. Considering that it is best practice to do a criticality analysis every two years or so, we recommend incremental improvements in the process. 

In two workshops, each lasting three hours, we completed the criticality analysis and identified about 30 highly critical pieces of equipment. With this, we had a starting point for our maintenance program.

2.	**Maintenance program**: Develop preventive and predictive maintenance plans

At this stage, the mission of the preventive maintenance program needs to be understood by all team members. With the PMs that we are creating, we intend to reduce any HSE-related risks and ensure that the equipment remains reliable throughout production.

The risk with some maintenance programs is that they can create a false sense of security. We often hear comments such as: “We are doing the PM on the equipment, but it still fails unexpectedly.” All too often, the PM program is built by the engineering team pre-commissioning, and most of the PM tasks are irrelevant or just too vague. 

This is where our library of detailed PM programs comes into play. Once we have identified high-criticality items and walked through the process to identify the main production equipment, we customize the inspections based on OEM manuals and based on boots-on-the-ground field inspections.

Finally, we built condition-based monitoring routes based on past practices in iron ore concentrators. The routes and PMs will need to be reviewed after 12-24 months of commissioning.

3.	**Spare parts review**: Identify critical spare parts for each piece of equipment

Spare parts and bill of materials (BOM) are the most common struggle points that we see in the majority of mining companies. In a new processing plant or a 20-year-old production line, problems related to BOM and unavailable spares cause a significant number of the delays in work order execution.

For this iron ore concentrator, we started by focusing on OEM information. When the EPCM contract is executed well and OEM spare parts are collected, it makes it very easy to compile the spare parts list. 

However, in several cases, we didn’t have the parts information; hence it required extensive field inspections and cross-referencing.

Partnership with reliable local suppliers becomes crucial in this process.

**Within six months, the team delivered the following results**:
- Identify critical equipment and align with the asset register
- Design the strategy and the maintenance program
- Develop more than 100 preventive maintenance templates
- Identify and check the spare parts essential for the continuity of operations
- Select suppliers requiring contracts for commissioning
- Complete and optimize data entry in SAP

In order to achieve sustainable results for the organization, our interventions are always adapted to the maturity level of the maintenance department.

#### *Learn more by [contacting us](/contact)*