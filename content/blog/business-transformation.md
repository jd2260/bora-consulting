+++
title = "Business Transformation"
date = "2021-03-24T13:50:46+02:00"
tags = ["Strategy"]
categories = ["Articles"]
description = "A hybrid approach to financial value creation"
banner = "img/blog/strategy.png"
+++

Since McKinsey’s RTS offering, we have witnessed a new wave of value creation offerings. The premises of transformational change are for the business to adapt to the new market or financial constraints. From a financial and operational point of view, the agility to play with the different levers is critical (especially during uncertain times), the transformational approach needs to be customized to the maturity of the organization. 

<!--more-->

{{<rawhtml>}}
    <section>
        <img class="center" src="/img/blog/strategy.png">
    </section>
{{</rawhtml>}}

In our clients’ business transformation journeys, the strategy remains the most critical step which will make it or break it. 

What exactly are we trying to achieve? Are we:  

- a) Targeting a transformation in our people’s mindset: cost consciousness, revenue increase, improvement in engagement care factor? 

or:

- b) Targeting mindset transformation while quantify & consolidating the initiatives within our financial data?

As much as both sounds very similar, these two strategies do not belong in the same league. Let me explain…

Mindset transformation of our people, from the cost of understanding to bringing improvement ideas to the management, requires a continuous improvement approach. Site-wide or targeted ideation sessions with key stakeholders (depending on the expected engagement level) will generate ideas. From our experience, these sessions remain a great way to bring the technical and hourly team up to speed on the business objectives. While exposing them to the financial and business targets, they will appreciate a more in-depth understanding of the business. We have seen some gems during these idea generation sessions. Through prioritization, we select key initiatives and empower the team to execute them. The focus is on the mindset and on the execution of the ideas. 

With the second approach, the financial constraints change the focus from the mindset to accounting. One of the recent transformation projects, we came in to continue delivering the transformation portfolio right after one of the top consulting firms. From high level, the framework is excellent. Gate system for the initiatives with the requirement at each stage keep the system robust. Financial focus of the framework makes it easier to calculate the return on investment. Similar to DMAIC approach, the system’s sole purpose was not only the execution of the initiatives, but to ensure that the gains from the initiatives remain locked and sustainable. 

Our observations proved that the second approach is very resource-hungry and difficult to sustain in the long term. The client tried to involve the technician as well as the low-level management. As noble as the idea is, the feedback and the pushback made the system unsustainable. Only a handful of users were using it and despite the company’s efforts to consolidate the data with financial documents, certain gaps could not be closed. 

#### *Learn more by [contacting us](/contact)* 
