+++
title = "Maintenance Maturity Assessment"
date = "2022-01-02T13:50:46+02:00"
tags = ["maitenance"]
categories = ["Articles"]
description = "Turnkey Maintenance Program within 4 Months"
banner = "img/blog/maturity_assesment.png"
+++

In early 2021, when wood prices were skyrocketing, we were asked to help a pulp and paper mill with over 700 employees in Quebec to increase availability by 20%. The plant was struggling with 55% OEE, and maintenance appeared to be the primary culprit.

<!--more-->

As part of our diagnosis phase, we started with a comprehensive gap analysis according to the benchmark.

In this article, we will cover the fundamentals of maintenance maturity assessment over a two-week period to develop a continuous improvement action plan.


### Approach

Using a systematic approach to assess the maintenance maturity of a company, we focus on three areas: systems, processes, and behaviors.

{{<rawhtml>}}
    <section>
        <img class="center" src="/img/blog/approach.png">
    </section>
{{</rawhtml>}}


### Maintenance scorecard

We’ve analyzed a variety of maintenance scorecards in the industry, and the most common problem was an all-consuming focus on the assessment process. This meant that the details of execution and specific how-to steps were often neglected.

To address these issues, we developed our proprietary maintenance scorecard model, focusing on quick diagnostic and incremental improvements.

To start the project, we asked the core maintenance team to complete a self-assessment. Initial analysis revealed that the assessments pointed to some clear trends. 

{{<rawhtml>}}
    <section>
        <img class="center" src="/img/blog/maturity_assesment.png">
    </section>
{{</rawhtml>}}

### Interviews with the team

We continued the assessment with structured interviews of the maintenance and operations teams. It is critical to listen to tradesmen to understand their perspectives. The most common complaint we hear is: “No one is listening.” 

Based on the initial results from the scorecard, we customized our questions and discussions focused on specific points aimed at discovering the root cause of the problem:
-	Safety issues
-	Availability drivers
-	Strengths, weaknesses, and opportunities
-	Work management challenges
-	Operating for reliability
-	Leadership and communication issues
-	Processes and reporting challenges

### Backlog analysis

Data analysis in ERP provides more depth to the assessment. The expectation is to find similarities between what we hear during the interviews and what we discover in ERP. 

Backlog analysis is a simple way to confirm our findings up to this point: constant firefighting due to poor planning and scheduling. 

After a quick analysis, we found that the backlog was over 26 weeks – with over 9,000 unplanned work orders! With eight planners, this represents 1,125 work orders to be administered per planner. With parts, kitting, and contractor management requirements, this number represents a high risk.

What does this mean for the system?

1.	The ERP needs to be cleaned to eliminate duplicates, wish-list items, and projects.
2.	A SWAT team approach is needed to get ahead of the breakdown wave: “The work order is in the system, but before we get to it, the equipment breaks down.”
3.	Corrective (emergency) work orders trump the planned work, which causes an increase in the backlog, and leads to a frustrated and disengaged maintenance crew; a breakdown crew can help the day shift trades to continue focusing on planned work orders and PMs.


### DILO (Day In the Life Of)

Go & See is one of the building blocks of Lean philosophy and it is impossible to do a proper assessment of maintenance management without boots-on-the-ground. 

We completed three DILO assessments with maintenance supervisors:
-	Frontline leadership strengths and weaknesses
-	Engagement levels on the floor level
-	Work execution rate and quality 
-	Tool time!


Here is an example of one of the DILO results:

{{<rawhtml>}}
    <section>
        <img class="center" src="/img/blog/dilo.png">
    </section>
    <section>
        <img class="center" src="/img/blog/results.png">
    </section>
{{</rawhtml>}}

### Results

After more than 20 formal interviews including management, supervisors, operators, and trades, and over 120 hours in plant with the employees to understand the current situation, we pinpointed the following root causes:

1.	**Systems**: CMMS is not leveraged to the max and poor document management controls
2.	**Processes**: Key processes are either missing or not reinforced (spare parts management, work management, reliability, shutdown management)
3.	**Behavior**: Inadequate amount of time spent by leadership in the field and accountability

### Next steps?

Assessments, reviews, and audits are a standard process for maintenance teams. However, they are more damaging than helpful when they produce no tangible results. 

Team members grow frustrated as auditors depart as quickly as they come in, leaving behind a long list of actions that need to be taken in their absence. 

Our action plan is based on one fundamental mindset: the power of incremental improvements for lasting results!


#### *Learn more by [contacting us](/contact)*