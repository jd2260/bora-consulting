+++
title = "Industrial Internet of Things"
date = "2021-03-24T13:50:46+02:00"
tags = ["technology"]
categories = ["Articles"]
description = "Buzzwords in Action, Helping Create Value"
banner = "img/blog/ABF.png"
+++

**Industry 4.0** refers to the 4th industrial revolution and is the transition to utilizing advanced computing for better manufacturing. Bernard Marr’s explanation of [Industry 4.0](https://www.youtube.com/watch?v=yKPrJJSv94M) is probably the best I have seen in social medias. 

<!--more-->

**Industrial Internet of Things** refers to the interconnectivity between objects in an industrial setting. This could be an actuator controlled remotely or a downstream process getting data from an earlier step. In everyday life (referred to as just IoT) this might mean using Google Home to turn on your house lights, stream something on your TV, or using your phone to start your car. 

Internet of Things is a component of Industry 4.0, but the terms are not interchangeable. IoT eliminates many inconveniences in our daily lives and has huge potential to greatly benefit industry.

### Case Study:

The brewing industry is an interesting case study of quickly adopting IIoT. Being a heavily regulated industry operating on tight margins, most owners lose sleep at night imagining critical infrastructure failure – and potentially the loss of their business. Many computer scientists, software engineers, and mechanics have left their day jobs to become a business owner and have utilized Agile methodologies to create tools for automating parts of the business. 

One owner utilized [Grafana](https://grafana.com/) – an open-source tool typically used for monitoring computing infrastructure, as a dynamic 24/7 monitoring platform to monitor tank pressures, temperatures and even the extent of fermentation. Other owners quickly became interested and he launched his own start-up, specializing in IoT for the craft beverage industry. Clients ranged from single person operations to multi-national conglomerates. 

These tools didn’t previously exist, so we utilized elements of industry 4.0 to help ease difficulties during processing. 

The following steps are a great approach to automating or adapting any component of Industry 4.0:

{{<rawhtml>}}
    <section>
        <img class="center" src="/img/blog/framework.png">
    </section>
{{</rawhtml>}}


Adopting Agile methodologies, we started making simple tools in a new position to make our day easier. These included automated KPI tracking, automated sampling compliance, and automated month end reporting. 

Contrary to popular belief, implementing Industry 4.0 initiatives does not need to be complicated. Agile principles state *“Simplicity – the art of maximizing the amount of work not done – is essential”* [1]. If building/implementing the tool takes more time than what you are saving, then it probably isn’t worth creating in the first place! Complex ideas can include digital item tagging, real-time batch analysis or using machine vision to automate visual inspections. These initiatives can mean long term cost savings and freeing up operators to perform other tasks. 

### Impact:

- Real-time batch analysis in breweries allowed brewers to be notified days before they typically would notice deviations resulting in improved tank availability
- Critical infrastructure monitoring alerted various breweries of refrigeration unit failures saving each brewery from thousands of dollars in lost product
- Simple analytical tools increased availability allowing more time to focus on cost savings projects
- Machine vision project resulted in removing an operator from a position allowing them to perform other duties resulting in increased inspection compliance and better operator availability

[1] Agile Manifesto, 2021 Available: https://agilemanifesto.org/principles.html


#### *Learn more by [contacting us](/contact)* 
