+++
title = "How to Reduce Energy Consumption & Reach Zero Emissions in Mining"
date = "2022-01-02T13:50:46+02:00"
tags = ["maitenance"]
categories = ["Articles"]
banner = "img/blog/equipment.jpg"
+++

The mining sector provides materials to a large portion of the global market. As the saying goes, if it’s not farmed, it is mined. However, the mining industry generates about [1.9 to 5.1 gigatons of CO2](https://www.tomorrowsworldtoday.com/2021/09/12/sustainable-mining-around-the-world/#:~:text=The%20mining%20industry%20generates%20between,emissions%20are%20increasing%20each%20year), negatively affecting the environment we live in. 

<!--more-->

{{<rawhtml>}}
    <section>
        <img class="center" src="/img/blog/equipment.jpg">
    </section>
{{</rawhtml>}}

Due to excessive energy consumption and CO2 emissions, the mining industry faces pressures to reduce its energy consumption. 
Mining companies need to examine renewable sources and reduce their dependency on external energy sources. Moreover, they need to take proper measures to reach zero emissions. Modern technology offers a lot of promise and can provide mining firms with the tools required to make their operations more sustainable. 

The ongoing efforts of a handful of companies are promising but not enough to achieve significant progress. Major sources of carbon emissions in mining include: 

-	Mapping/Surveying 
-	Transportation of Resources
-	Energy Use for Equipment 
-	Energy Use for Mining Camp Laborer Accommodation (Mining Camps)

In order for the mining industry to be considered sustainable, mining leaders should address these areas and implement the use of emerging clean technologies.

If you’re in the mining and metals industry and interested in learning how to reduce energy consumption and reach zero emissions in the future, read on. 

#### Choose an Energy Management System and Strategy
A proactive power management technique makes use of systems that are built to consume less energy. A dynamic power management plan focuses on installing and maintaining motor controls, air controls, or operational efficiencies for the best outcome.

Select an energy management system (EMS) that is immediately correlated to the mine/plant’s production systems. For example, FLSmidth offers an energy management system that can assist in more advanced monitoring of energy use to drive down costs. 

Some EMS solutions can enumerate consumption that outstrips targets, measures critical success factors, offers additional validated evidence to confirm future investments, and generates reports and electricity models. They can help anticipate energy usage, evaluate efficiency goals, and provide information about real-time electricity usage.

#### Use Sustainable Haulage Trucks
Decarbonization of haulage trucks could cut a mine’s pollutants by as much as 25%. Consequently, this is among the most important steps to take. The use of renewable fuels such as biofuels will become the most important option in the short run. Converting from today’s diesel-powered trucks to either pantograph-charged BEV or hydrogen-powered trucks can provide significant energy savings. 

As part of its FutureSmart Mining™ innovation program, Anglo American is developing the world’s largest hydrogen-powered haul truck.
Newmont Goldcorp officially opened an all-electric mine in Canada in 2019, and Gold Fields in Australia now meets over half its energy needs through renewable sources after backing up its hybrid power microgrid with a lithium-ion battery energy storage system (BESS) in 2020

#### Reuse Waste
Waste from mining, such as ashes, boulders, and wastewater, is an unavoidable part of the business. When mining operations are finished, many corporations leave trash behind, which causes severe damage to the environment. This can be prevented by using waste rocks for minor on-site building tasks such as patching voids and re-creating mined landscapes in a way that avoids soil erosion.

Water used in mining operations, if properly treated, can be utilized in different ways. It can be employed as a coolant or used for agriculture. The mining industry can become a truly sustainable enterprise by paying attention to the tiny details of the products utilized and manufactured at a mine site.

Even tailings that are left behind in sites can be used efficiently. Depending on their chemical composition, they may be used in agroforestry, as paint extenders, or for making bricks.

Some advanced technologies make it possible to mine further from the tailings. This reduces the number of minerals left behind in sites while reducing the amount of waste generated during the mining process. 

Nevertheless, not all the ideas are financially viable at this time. To scale up some of the technologies mentioned in this article, the mining sector will probably need to invest in more research and innovation in the field of mine waste recycling.

#### Recycle tires
Tires are one of the final frontiers of waste management at mine sites.

Traditionally, shredded mining tires have sometimes been shipped to countries with less restrictive emissions standards to be burned and then used as tire-derived fuel to replace coal.

In other cases, used tires have simply gone to ground as landfill with no other alternatives being available.

“Mining companies don’t pile their garbage up but with tires it has been okay up until now because there has not been a solution,” one industry leader commented in Australian Mining.

“We really want to get a circular economy going with mining tires. Scrap tires are a valuable commodity because of what is in the tire. This process (thermal conversion) can extract the materials.”

A 63-inch mining tire, for example, comprises four key materials after thermal conversion – carbon black (1600 kilograms), steel (750 kilograms), oil (1900 litres), and gas (350 cubed meters).

The oil can be upgraded to diesel, which could potentially fuel the mining trucks that had the tires previously running on them.
Carbon black also has many uses, the main being used in the production of tires and plastics. Another use that stands out is its value as an ingredient in rechargeable batteries, an emerging driver of mining machinery.

#### Adopt Green Mining Technology
The mining sector is constantly in need of adequate innovation to keep up with today’s approach to sustainability and environmentally friendly practices.

Gathering funding and allowing it to be disbursed into research and development for Green Mining either through provincial or national authorities can be one strategy to help responsibly address climate issues before and after mining ventures. Staying ahead of the game will help you avoid unnecessary wastage, reduce energy consumption, and eventually reach the goal of zero emissions in mining. 

Check out one of the [Venture Capital studios](www.greenlightpartners.nyc) that solely invests in Green/Clean Tech startups to help the mining and metals industry in their journey to a green and sustainable future. 

#### Use Selective Smart Blasting 
Effective blasting provides an opportunity to increase productivity and efficiency on mining sites. Conventional blasting focuses on the mine’s entire region to achieve the maximum size that can be taken in haul trucks and processed with the primary crusher. Selective smart blasts utilize geometallurgical data to target sections with high ore concentrations with more powerful blast energy. 

This improves the grade of ore being supplied to the crusher, and the total energy utilized in crushing and grinding phases is reduced. [Research](http://businessdocbox.com/Metals/81898704-The-effects-of-blasting-on-crushing-and-grinding-efficiency-and-energy-consumption.html) shows that mining companies can achieve savings of about 30% through selective smart blasting. 

#### Use Advanced Grinding Technologies 
[Studies](https://espace.library.uq.edu.au/view/UQ:188060) have revealed that in certain mines, the total energy that goes into the grinding process can be reduced by 40% with the use of the latest equipment. Computer simulations with the discrete element method have revealed that most rocks that are larger than discharge grate size don’t break down in the initial collision. Instead, it takes multiple collisions to break them, which is a less efficient use of energy. 

A wide variety of comminution equipment is now available for different materials and for various conditions. It is essential to use the right equipment depending on the material and condition for a more energy-efficient crushing. Using fine grinding equipment and energy-efficient crushing can reduce energy use by reducing the need to use grinding media with high embodied energy. This reduces the secondary and primary recirculating loads, leading to reduced power requirements.

### Final Thoughts

Natural resources are essential to our economy, yet extracting them via mining operations takes a toll on the environment because these processes consume a lot of energy and generate a significant amount of waste. 

As carbon costs increase, regulations stiffen, investor pressure grows, and firms strive to meet their objectives, emissions reduction remains a critical topic in the continuous development circle. Setting the correct foundation for attaining those objectives over the lifespan of any carbon-producing asset is critical, and adopting proactive efforts today can help ensure a more seamless transition to a carbon-free future.


#### *To learn more about how we can help you improve your operational efficiency and performance, please [reach out to us](/contact)*