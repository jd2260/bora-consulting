+++
title = "Conveyor belts – Mechanical Splicing vs. Vulcanizing"
date = "2022-01-02T13:50:46+02:00"
tags = ["process"]
categories = ["Articles"]
description = "Turnkey Maintenance Program within 4 Months"
banner = "img/blog/truck.png"
+++

#### About the author
Mark Colbourn is the managing director of Flexco Australia. Article first published in the April 2018 issue of Quarry Management as Mechanical Splicing vs Vulcanizing, the full article can be read [here](https://www.agg-net.com/resources/articles/materials-handling/conveyor-belt-mechanical-splicing-vs-vulcanizing).

<!--more-->

Conveyor systems are at the heart of most quarries and mines, and with extractive operations doing everything they can to improve operating efficiencies and reduce costs, they are excellent first checkpoint when seeking to optimize productivity. A critical area of importance on any conveyor system is the splice, and in most applications, there are two preferred types of splicing method: mechanical splicing, which joins belt ends by metal hinges or plates; and vulcanized splicing, which joins belt ends through heat and/or chemicals.

Understanding the pros and cons of each method is extremely important when making an educated decision on which splicing method to use. What are the environmental factors that might affect the splice? Are workers up against time constraints? What are the costs associated with long conveyor downtimes? These are just some of the questions to ask when choosing between mechanical fastening and vulcanization.

### Vulcanization process
Vulcanization is a complex procedure that, if done correctly by an expert, can offer a smooth splice with minimal risk of snagging, tearing and other harmful wear to the belt. 

For both hot and cold vulcanization, the belt must be disassembled and each belt end prepared according to that particular belt’s splicing recommendation. Proper belt preparation is crucial to ensure the finished splice will hold to its published tensile ratings.
With hot vulcanization, splices are heated and cured under pressure with a vulcanizing press. This process takes several hours. If a belt is re-tensioned and used before the splice is bonded and completely cooled, the splice will be ineffective and may come apart completely, causing additional downtime.

Cold vulcanization does not employ a vulcanizing press, but instead uses a bonding agent that causes a chemical reaction to splice the two belt ends together.

When vulcanizing, several factors must be considered to ensure a high-quality splice:
First, an expert who is skilled and trained in the procedure and who has a thorough knowledge of solvents, bonding materials and other cover and fill materials must perform a vulcanized splice.

Secondly, the process requires a specific temperature, compression and equipment dwell time, in addition to a virtually moisture-free work area.

Thirdly, some types of belt may not allow for vulcanization. If the belt is old, dirty or unevenly worn, vulcanization is not a good option because it will not always cure evenly, which can result in a weaker splice.

Vulcanized splicing of a 600mm wide belt could take between 6 and 11 hours, depending on working conditions. Wider belts may take longer. And because vulcanization often requires time for a specialized vulcanizing crew and equipment to be brought on site, operations can be shut down for half a day or more.

To summarize, vulcanized splicing can be used where:
-	The belt is clean and free of contaminating agents, such as oil, sand and material fines
-	The belt is compatible with the adhesive of choice
-	The belt is new or without excessive wear
-	The procedure is performed by a trusted, certified vulcanizer
-	The work environment is at an optimal temperature and moisture level 
-	There is easy access to the area that needs splicing and plenty of room to work
-	There is enough downtime available to allow for a properly installed vulcanized splice.

### Mechanical splicing
Mechanical splice installation is quick and simple. Depending on belt width and thickness, most mechanical splices can be finished in less than one hour and are installed by an in-house crew with portable, easy-to-use installation tools.

If an unexpected splice is required, it is not necessary to wait for professional assistance. In addition, mechanical splices can be made in restrictive environments with no special regard for space, temperature, moisture or contaminants.

Mechanical splicing also offers reduced belt waste which can significantly reduce costs. Because vulcanized splices often require the consumption of 2–3m of belt length, conveyors may not have enough ‘take-up’ if more than one splice is necessary over time.

Moreover, because a mechanical splice is visible, wear and deterioration is apparent and can be taken care of before a complete belt failure. Vulcanized splices, in contrast, typically deteriorate from the inside out due to poor adhesion. The first sign of wear comes too late for any preventative measure, resulting in longer downtime.

As with vulcanization, there are several types of mechanical fastener, each created for use with different belt widths, lengths, thicknesses, speeds, tensions and belt cleaners.

No matter what the belt condition, mechanical fasteners are a good choice for both new and older, worn belts. Rivet-hinged fasteners can be used on belt thicknesses ranging from 3mm to 25mm with minimum pulley diameters of 230mm.

Because removing the hinge pin can easily separate hinged fasteners, these designs are essential in mining and quarrying applications where belts must frequently be removed, extended or shortened. In addition, hinged fasteners provide several installation benefits in these applications.
Mechanical splice installation tools are easily transported to the job site and offer splice installers versatility in installation methods. Depending on the site’s available power source, mechanically attached rivet-hinged splices can be installed with as little as a basic installation tool and hammer, or with a modified installation tool and choice of electric or air-actuated power source.

Besides virtually eliminating fastener rip-outs, rivet-hinged splicing cuts downtime by giving maintenance crews more freedom in deciding when to replace a splice. Any splice damage or wear and tear is very visible on a mechanical splice, and operators can finish a shift even with a few plates missing and not have to worry about belt failure.

### Vulcanization vs Fastening – some common misconceptions
Every splicing method has its limitations, and it is essential to get the facts before deciding how best to splice a belt. Some of the most common misconceptions include:

*‘Mechanical fasteners cannot be used with higher-tension belts’* (i.e., more than 800 PIW)  
Synthetic belts and improved fastener designs have resulted in mechanical fasteners that are compatible with belt tension ratings of up to 350kN/m (2,000 PIW).

*‘Mechanical fasteners are noisy, incompatible with belt cleaners and scrapers, and generally damaging to the belt’*  
If mechanical splices are properly installed, maintained and countersunk by skiving the belt, there should be no problem with noise or damage to the belt or belt cleaners.

*‘All belts can be vulcanized’*  
Old and/or worn fabric belts are not well suited to vulcanization because the belt layers are weaker and will become brittle when heat is applied. Finally, vulcanizing requires additional belt length, so operations with little take-up simply may not have enough belt to vulcanize.

*‘Vulcanization does not mean a lot of downtime’*  
Vulcanization actually requires the shutdown of the belt for a substantial amount of time – much longer than a mechanical splice would require. Not only do the chemicals take several hours to cure, but a vulcanized splice is also at the mercy of the vulcanizer’s schedule.

*‘Vulcanization does not compromise belt strength’*  
Vulcanizing actually robs the belt of an entire ply of strength – even more if not done properly. Mechanical fastening, on the other hand, does not compromise the belt’s integrity.

*‘Inspecting a vulcanized splice is easy’*  
The early signs of adhesion breakdown in a vulcanized splice are nearly invisible to the naked eye. Often, operators are not even aware that a vulcanized splice is experiencing problems until it fails – a catastrophic event that requires the immediate shutdown of the line.

### Fact of life
Conveyor belt and belt splice damage will always be a fact of life in most bulk material handling applications. Consequently, operations and maintenance personnel should have a thorough understanding of the available splicing and repair alternatives, and how each method can affect the productivity and cost-effectiveness of operations.

New designs, materials and processes are making mechanical splicing better, and incorporating mechanical belt fasteners into a splicing routine can provide numerous benefits for output and bottom line. In most applications, mechanical splices offer the flexibility, economy and speed needed to minimize material and labour costs and avoid expensive downtime situations.


#### *Learn more by [contacting us](/contact)*